﻿using System.Collections.Generic;

public class Node
{
    public List<int> Nodes = new List<int>();

    public Node(int node1, int node2)
    {
        Nodes.Add(node1);
        Nodes.Add(node2);
    }
}