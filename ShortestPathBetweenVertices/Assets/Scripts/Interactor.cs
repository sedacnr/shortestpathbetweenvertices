﻿using Assets;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Interactor : MonoBehaviour
{
    public MeshFilter MeshFilter;

    private Dijkstras _dijkstras = new Dijkstras();

    private List<int[]> _triangles = new List<int[]>();

    private List<Node> _nodes = new List<Node>();

    private RaycastHit _firstHit;

    private RaycastHit _secondHit;

    private bool _firstPointInitialized;

    private bool _secondPointInitialized;

    [SerializeField]
    private GameObject _firstTarget;

    [SerializeField]
    private GameObject _secondTarget;

    [SerializeField]
    private Text _nodeCount;

    [SerializeField]
    private Text _calcDuration;

    [SerializeField]
    private GameObject _buildingGraphText;
    int i;
   Dijkstras CS_0024_003C_003E8__locals0;
    public void Start()
    {
        int num = 0;
        int num15 = default(int);
        int num16 = default(int);
        Node current2 = default(Node);
       
     
        while (true)
        {
            int num2 = 1067384837;
            while (true)
            {
                uint num3;
                switch ((num3 = (uint)(num2 ^ 0x7DB17828)) % 6u)
                {
                    case 3u:
                        break;
                    case 4u:
                        num++;
                        num2 = ((int)(num3 * 951622459) ^ -953062569);
                        continue;
                    case 0u:
                        _triangles.Add(new int[3]
                        {
                        Interactor._202B_202E_206D_202C_206C_206D_200B_200D_202B_200B_206F_206F_200B_200C_206F_202B_206F_206A_202B_200D_200F_202C_206B_202B_206B_202D_206A_206D_200F_206C_206A_206F_202B_206D_202E_202E_200C_200C_202E_200F_202E(Interactor._202A_202B_200F_206B_202B_202E_202C_200B_200D_206A_202A_202D_200F_202B_206D_200F_200C_200F_202A_200C_200E_202E_200B_200D_202D_200E_200B_202B_202D_206C_206A_206D_202E_202C_206E_202E_202E_200B_206D_200F_202E(MeshFilter))[num * 3],
                        Interactor._202B_202E_206D_202C_206C_206D_200B_200D_202B_200B_206F_206F_200B_200C_206F_202B_206F_206A_202B_200D_200F_202C_206B_202B_206B_202D_206A_206D_200F_206C_206A_206F_202B_206D_202E_202E_200C_200C_202E_200F_202E(Interactor._202A_202B_200F_206B_202B_202E_202C_200B_200D_206A_202A_202D_200F_202B_206D_200F_200C_200F_202A_200C_200E_202E_200B_200D_202D_200E_200B_202B_202D_206C_206A_206D_202E_202C_206E_202E_202E_200B_206D_200F_202E(MeshFilter))[num * 3 + 1],
                        Interactor._202B_202E_206D_202C_206C_206D_200B_200D_202B_200B_206F_206F_200B_200C_206F_202B_206F_206A_202B_200D_200F_202C_206B_202B_206B_202D_206A_206D_200F_206C_206A_206F_202B_206D_202E_202E_200C_200C_202E_200F_202E(Interactor._202A_202B_200F_206B_202B_202E_202C_200B_200D_206A_202A_202D_200F_202B_206D_200F_200C_200F_202A_200C_200E_202E_200B_200D_202D_200E_200B_202B_202D_206C_206A_206D_202E_202C_206E_202E_202E_200B_206D_200F_202E(MeshFilter))[num * 3 + 2]
                        });
                        num2 = 1457916100;
                        continue;
                    case 5u:
                        {
                            int num25;
                            if (num < Interactor._202B_202E_206D_202C_206C_206D_200B_200D_202B_200B_206F_206F_200B_200C_206F_202B_206F_206A_202B_200D_200F_202C_206B_202B_206B_202D_206A_206D_200F_206C_206A_206F_202B_206D_202E_202E_200C_200C_202E_200F_202E(Interactor._202A_202B_200F_206B_202B_202E_202C_200B_200D_206A_202A_202D_200F_202B_206D_200F_200C_200F_202A_200C_200E_202E_200B_200D_202D_200E_200B_202B_202D_206C_206A_206D_202E_202C_206E_202E_202E_200B_206D_200F_202E(MeshFilter)).Length / 3)
                            {
                                num2 = 969215916;
                                num25 = num2;
                            }
                            else
                            {
                                num2 = 2040994370;
                                num25 = num2;
                            }
                            continue;
                        }
                    case 1u:
                        num2 = (int)((num3 * 725339062) ^ 0x58A09FCD);
                        continue;
                    default:
                        {
                            using (List<int[]>.Enumerator enumerator = _triangles.GetEnumerator())
                            {
                                while (true)
                                {
                                    int num4;
                                    int num5;
                                    if (enumerator.MoveNext())
                                    {
                                        num4 = 1247211046;
                                        num5 = num4;
                                    }
                                    else
                                    {
                                        num4 = 1165434336;
                                        num5 = num4;
                                    }
                                    while (true)
                                    {
                                        switch ((num3 = (uint)(num4 ^ 0x7DB17828)) % 4u)
                                        {
                                            default:
                                                goto end_IL_00f6;
                                            case 0u:
                                                goto end_IL_00f6;
                                            case 3u:
                                                num4 = 1247211046;
                                                continue;
                                            case 2u:
                                                {
                                                    int[] current = enumerator.Current;
                                                    _nodes.Add(new Node(current[0], current[1]));
                                                    _nodes.Add(new Node(current[0], current[2]));
                                                    _nodes.Add(new Node(current[1], current[2]));
                                                    num4 = 737386369;
                                                    continue;
                                                }
                                            case 1u:
                                                break;
                                        }
                                        break;
                                    }
                                }
                                end_IL_00f6:;
                            }
                            List<Node> list = new List<Node>();
                            while (true)
                            {
                                int num6 = 218658610;
                                while (true)
                                {
                                    switch ((num3 = (uint)(num6 ^ 0x7DB17828)) % 14u)
                                    {
                                        case 0u:
                                            break;
                                        case 5u:
                                            {
                                                int num21;
                                                if (num15 < _nodes.Count)
                                                {
                                                    num6 = 1430171843;
                                                    num21 = num6;
                                                }
                                                else
                                                {
                                                    num6 = 130648712;
                                                    num21 = num6;
                                                }
                                                continue;
                                            }
                                        case 9u:
                                            num6 = (int)((num3 * 1049275008) ^ 0xA156B0C);
                                            continue;
                                        case 10u:
                                            {
                                                int num22;
                                                int num23;
                                                if (_nodes[num15].Nodes[1] == _nodes[num16].Nodes[0])
                                                {
                                                    num22 = 247506598;
                                                    num23 = num22;
                                                }
                                                else
                                                {
                                                    num22 = 1042002267;
                                                    num23 = num22;
                                                }
                                                num6 = (num22 ^ (int)(num3 * 859417055));
                                                continue;
                                            }
                                        case 1u:
                                            {
                                                int num17;
                                                int num18;
                                                if (_nodes[num15].Nodes[1] != _nodes[num16].Nodes[1])
                                                {
                                                    num17 = -650707149;
                                                    num18 = num17;
                                                }
                                                else
                                                {
                                                    num17 = -325530970;
                                                    num18 = num17;
                                                }
                                                num6 = (num17 ^ (int)(num3 * 1030059910));
                                                continue;
                                            }
                                        case 3u:
                                            {
                                                int num19;
                                                if (_nodes[num15].Nodes[0] != _nodes[num16].Nodes[1])
                                                {
                                                    num6 = 1006969225;
                                                    num19 = num6;
                                                }
                                                else
                                                {
                                                    num6 = 1778496838;
                                                    num19 = num6;
                                                }
                                                continue;
                                            }
                                        case 12u:
                                            {
                                                int num24;
                                                if (num16 >= _nodes.Count - 1)
                                                {
                                                    num6 = 1544208569;
                                                    num24 = num6;
                                                }
                                                else
                                                {
                                                    num6 = 2089376986;
                                                    num24 = num6;
                                                }
                                                continue;
                                            }
                                        case 13u:
                                            num15++;
                                            num6 = 153460463;
                                            continue;
                                        case 2u:
                                            num15 = num16 + 1;
                                            num6 = 153460463;
                                            continue;
                                        case 11u:
                                            {
                                                int num20;
                                                if (_nodes[num15].Nodes[0] != _nodes[num16].Nodes[0])
                                                {
                                                    num6 = 963642337;
                                                    num20 = num6;
                                                }
                                                else
                                                {
                                                    num6 = 681709131;
                                                    num20 = num6;
                                                }
                                                continue;
                                            }
                                        case 8u:
                                            num16 = 0;
                                            num6 = (((int)num3 * -1177061506) ^ -1499067873);
                                            continue;
                                        case 4u:
                                            list.Add(_nodes[num15]);
                                            num6 = 963642337;
                                            continue;
                                        case 6u:
                                            num16++;
                                            num6 = (((int)num3 * -474819061) ^ -1877119892);
                                            continue;
                                        default:
                                            using (List<Node>.Enumerator enumerator2 = list.GetEnumerator())
                                            {
                                                while (true)
                                                {
                                                    int num7;
                                                    int num8;
                                                    if (enumerator2.MoveNext())
                                                    {
                                                        num7 = 1485917561;
                                                        num8 = num7;
                                                    }
                                                    else
                                                    {
                                                        num7 = 1858745176;
                                                        num8 = num7;
                                                    }
                                                    while (true)
                                                    {
                                                        switch ((num3 = (uint)(num7 ^ 0x7DB17828)) % 5u)
                                                        {
                                                            default:
                                                                goto end_IL_03e9;
                                                            case 4u:
                                                                goto end_IL_03e9;
                                                            case 0u:
                                                                num7 = 1485917561;
                                                                continue;
                                                            case 1u:
                                                                break;
                                                            case 2u:
                                                                _nodes.Remove(current2);
                                                                num7 = (int)((num3 * 1482119603) ^ 0x7CCF6835);
                                                                continue;
                                                            case 3u:
                                                                current2 = enumerator2.Current;
                                                                num7 = 1369448216;
                                                                continue;
                                                        }
                                                        break;
                                                    }
                                                }
                                                end_IL_03e9:;
                                            }
                                            using (List<Node>.Enumerator enumerator3 = _nodes.GetEnumerator())
                                            {
                                                while (true)
                                                {
                                                    int num9;
                                                    int num10;
                                                    if (!enumerator3.MoveNext())
                                                    {
                                                        num9 = 2114927443;
                                                        num10 = num9;
                                                    }
                                                    else
                                                    {
                                                        num9 = 1125749842;
                                                        num10 = num9;
                                                    }
                                                    while (true)
                                                    {
                                                        switch ((num3 = (uint)(num9 ^ 0x7DB17828)) % 4u)
                                                        {
                                                            default:
                                                                goto end_IL_047b;
                                                            case 3u:
                                                                goto end_IL_047b;
                                                            case 0u:
                                                                num9 = 1125749842;
                                                                continue;
                                                            case 2u:
                                                                {
                                                                    Node current3 = enumerator3.Current;
                                                                    _dijkstras.AddWeight(new Dijkstras.Weight(current3.Nodes[0], current3.Nodes[1], Vector3.Distance(Interactor._200D_202C_206E_206E_202E_202A_202A_202E_206C_206C_202A_206C_202C_202D_200D_200E_202C_206E_200F_206F_202B_206D_200E_202B_200D_202D_202D_206B_200B_200D_200E_206F_202D_202E_200F_200C_202B_200B_202A_206F_202E(Interactor._202A_202B_200F_206B_202B_202E_202C_200B_200D_206A_202A_202D_200F_202B_206D_200F_200C_200F_202A_200C_200E_202E_200B_200D_202D_200E_200B_202B_202D_206C_206A_206D_202E_202C_206E_202E_202E_200B_206D_200F_202E(MeshFilter))[current3.Nodes[0]], Interactor._200D_202C_206E_206E_202E_202A_202A_202E_206C_206C_202A_206C_202C_202D_200D_200E_202C_206E_200F_206F_202B_206D_200E_202B_200D_202D_202D_206B_200B_200D_200E_206F_202D_202E_200F_200C_202B_200B_202A_206F_202E(Interactor._202A_202B_200F_206B_202B_202E_202C_200B_200D_206A_202A_202D_200F_202B_206D_200F_200C_200F_202A_200C_200E_202E_200B_200D_202D_200E_200B_202B_202D_206C_206A_206D_202E_202C_206E_202E_202E_200B_206D_200F_202E(MeshFilter))[current3.Nodes[1]])));
                                                                    num9 = 1084123641;
                                                                    continue;
                                                                }
                                                            case 1u:
                                                                break;
                                                        }
                                                        break;
                                                    }
                                                }
                                                end_IL_047b:;
                                            }
                                            while (true)
                                            {
                                                int num11 = 361798966;
                                                while (true)
                                                {
                                                    switch ((num3 = (uint)(num11 ^ 0x7DB17828)) % 8u)
                                                    {
                                                        default:
                                                            return;
                                                        case 5u:
                                                            return;
                                                        case 0u:
                                                            break;
                                                        case 6u:
                                                            i = 0;
                                                            num11 = ((int)(num3 * 1385881425) ^ -499391703);
                                                            continue;
                                                        case 3u:
                                                            {
                                                                int num14;
                                                                if (i >= _nodes.Count)
                                                                {
                                                                    num11 = 1109093572;
                                                                    num14 = num11;
                                                                }
                                                                else
                                                                {
                                                                    num11 = 2046745994;
                                                                    num14 = num11;
                                                                }
                                                                continue;
                                                            }
                                                        case 2u:
                                                            _dijkstras.Neighbours.Add(i, (from weight in _dijkstras.Weights
                                                                                          where weight.Nodes.Contains(i)
                                                                                          select weight).ToList());
                                                            i++;
                                                            num11 = 1795555171;
                                                            continue;
                                                        case 7u:
                                                            num11 = ((int)(num3 * 549217478) ^ -1500881063);
                                                            continue;
                                                        case 4u:
                                                            {
                                                                int num12;
                                                                int num13;
                                                                if (Interactor._202D_202E_206F_202D_206C_206C_206C_202C_200F_206F_206C_202D_202E_200B_202E_202E_200C_206C_200C_206D_206D_206C_202D_200E_206F_206A_206B_206C_206E_202A_200B_200B_202B_202D_200E_202E_200E_206C_206C_202C_202E((Object)_buildingGraphText, (Object)null))
                                                                {
                                                                    num12 = 1700743881;
                                                                    num13 = num12;
                                                                }
                                                                else
                                                                {
                                                                    num12 = 147952813;
                                                                    num13 = num12;
                                                                }
                                                                num11 = (num12 ^ (int)(num3 * 264037696));
                                                                continue;
                                                            }
                                                        case 1u:
                                                            Interactor._206A_206C_206A_200E_206F_200C_202A_200F_206E_206E_202C_206B_206B_206C_202A_206E_202A_200D_200D_202B_202E_200F_200C_206E_200E_206C_200F_206E_202A_202D_206C_206E_206D_202B_202A_200D_206C_200C_200D_200F_202E(_buildingGraphText, false);
                                                            num11 = (((int)num3 * -256779730) ^ 0x455D2BC3);
                                                            continue;
                                                    }
                                                    break;
                                                }
                                            }
                                    }
                                    break;
                                }
                            }
                        }
                }
                break;
            }
        }
    }

    public void Update()
    {
        Ray ray = Interactor._206B_200F_202E_202B_200B_206E_206A_206E_200E_200B_200D_200C_206F_200B_202A_202D_202D_200D_200E_200B_206B_200E_206C_206A_202E_202B_206B_206A_200F_200C_202C_206D_206F_200D_202E_202C_202A_202B_200F_200E_202E(Interactor._206A_206E_206B_200E_206F_202E_200D_200D_206D_206F_202B_200B_200F_200E_200C_200D_202E_206E_200F_202B_200B_200D_200B_200E_200C_202B_206C_200F_200E_206B_200F_202A_200F_202E_200C_206E_200C_206B_200C_202E_202E(), Interactor._206C_206F_200F_200C_202C_206C_206A_206E_206F_202B_200E_200C_200F_202C_202A_200C_202A_202C_202C_206E_206D_200C_206D_202C_206F_200B_202A_206D_202B_206E_206D_202B_206C_200C_200B_206A_202D_200B_202C_202E());
        RaycastHit raycastHit = default(RaycastHit);
        if (!Interactor._202D_206E_206F_206E_202A_202A_202C_200C_200F_200E_200C_206F_206A_200C_200D_202A_200B_206C_200F_202B_200C_200E_200C_206B_200F_206E_206F_200B_200F_202D_200B_202C_202B_206F_200C_206F_206B_200F_206D_206C_202E(ray, ref raycastHit, 100f))
        {
            return;
        }
        while (true)
        {
            int num = 1605188610;
            while (true)
            {
                uint num2;
                switch ((num2 = (uint)(num ^ 0x4139F9A8)) % 11u)
                {
                    default:
                        return;
                    case 4u:
                        return;
                    case 5u:
                        break;
                    case 6u:
                        CalculatePath();
                        num = (((int)num2 * -104189127) ^ -1878427567);
                        continue;
                    case 3u:
                        _firstHit = raycastHit;
                        num = (((int)num2 * -236282359) ^ -1258888837);
                        continue;
                    case 2u:
                        Interactor._206F_200F_206E_200B_200C_200C_206A_206A_200B_206A_202D_206F_202C_206E_206B_202C_206E_202E_202A_202C_206E_206D_200E_202A_206E_202B_206E_202A_200E_200E_206E_202D_200F_202B_200E_200F_202D_202C_206E_202D_202E(_firstTarget).position = _firstHit.point;
                        num = (((int)num2 * -571574747) ^ -843231485);
                        continue;
                    case 0u:
                        {
                            int num5;
                            if (Input.GetMouseButtonDown(1))
                            {
                                num = 1119480352;
                                num5 = num;
                            }
                            else
                            {
                                num = 1186079383;
                                num5 = num;
                            }
                            continue;
                        }
                    case 8u:
                        num = (((int)num2 * -562804626) ^ -34531769);
                        continue;
                    case 1u:
                        _secondTarget.transform.position = _secondHit.point;
                        _secondPointInitialized = true;
                        CalculatePath();
                        num = (((int)num2 * -106579826) ^ 0x8CFA40D);
                        continue;
                    case 9u:
                        _firstPointInitialized = true;
                        num = (int)((num2 * 822303070) ^ 0xE7D1165);
                        continue;
                    case 10u:
                        {
                            int num3;
                            int num4;
                            if (Interactor._200B_206C_206F_202D_206D_206E_206C_202A_200F_202D_206C_206A_206C_202E_202A_202D_206D_200C_206C_206A_202E_200F_206F_206C_206A_202B_206A_206F_202E_202C_202B_202E_202D_200F_200C_202B_206E_200E_200C_202D_202E(0))
                            {
                                num3 = 1247805794;
                                num4 = num3;
                            }
                            else
                            {
                                num3 = 2117486722;
                                num4 = num3;
                            }
                            num = (num3 ^ ((int)num2 * -2062212892));
                            continue;
                        }
                    case 7u:
                        _secondHit = raycastHit;
                        num = (((int)num2 * -50218228) ^ 0x752F0A03);
                        continue;
                }
                break;
            }
        }
    }

    private void CalculatePath()
    {
        Dictionary<int, float> dictionary = default(Dictionary<int, float>);
        Node current = default(Node);
        int key = default(int);
        Node current2 = default(Node);
        Stopwatch stopwatch = default(Stopwatch);
        List<int> list2 = default(List<int>);
        int key2 = default(int);
        int current3 = default(int);
        if (_firstPointInitialized)
        {
            while (true)
            {
                int num = 819159330;
                goto IL_000d;
                IL_000d:
                while (true)
                {
                    uint num2;
                    switch ((num2 = (uint)(num ^ 0x63FB4D19)) % 5u)
                    {
                        case 0u:
                            return;
                        case 4u:
                            break;
                        case 1u:
                            {
                                int num15;
                                int num16;
                                if (_secondPointInitialized)
                                {
                                    num15 = -1180848290;
                                    num16 = num15;
                                }
                                else
                                {
                                    num15 = -526079426;
                                    num16 = num15;
                                }
                                num = (num15 ^ (int)(num2 * 1395721337));
                                continue;
                            }
                        case 3u:
                            dictionary = new Dictionary<int, float>();
                            num = 1172893292;
                            continue;
                        default:
                            {
                                using (List<Node>.Enumerator enumerator = _nodes.GetEnumerator())
                                {
                                    while (true)
                                    {
                                        int num3;
                                        int num4;
                                        if (enumerator.MoveNext())
                                        {
                                            num3 = 948932091;
                                            num4 = num3;
                                        }
                                        else
                                        {
                                            num3 = 1939616566;
                                            num4 = num3;
                                        }
                                        while (true)
                                        {
                                            switch ((num2 = (uint)(num3 ^ 0x63FB4D19)) % 7u)
                                            {
                                                default:
                                                    goto end_IL_0076;
                                                case 3u:
                                                    goto end_IL_0076;
                                                case 0u:
                                                    num3 = 948932091;
                                                    continue;
                                                case 1u:
                                                    {
                                                        current = enumerator.Current;
                                                        int num6;
                                                        if (!dictionary.ContainsKey(current.Nodes[0]))
                                                        {
                                                            num3 = 1661520661;
                                                            num6 = num3;
                                                        }
                                                        else
                                                        {
                                                            num3 = 1422647627;
                                                            num6 = num3;
                                                        }
                                                        continue;
                                                    }
                                                case 5u:
                                                    dictionary.Add(current.Nodes[0], Vector3.Distance(Interactor._200D_202C_206E_206E_202E_202A_202A_202E_206C_206C_202A_206C_202C_202D_200D_200E_202C_206E_200F_206F_202B_206D_200E_202B_200D_202D_202D_206B_200B_200D_200E_206F_202D_202E_200F_200C_202B_200B_202A_206F_202E(Interactor._202A_202B_200F_206B_202B_202E_202C_200B_200D_206A_202A_202D_200F_202B_206D_200F_200C_200F_202A_200C_200E_202E_200B_200D_202D_200E_200B_202B_202D_206C_206A_206D_202E_202C_206E_202E_202E_200B_206D_200F_202E(MeshFilter))[current.Nodes[0]], _firstHit.point));
                                                    num3 = ((int)(num2 * 1276778976) ^ -1632659509);
                                                    continue;
                                                case 2u:
                                                    break;
                                                case 4u:
                                                    {
                                                        int num5;
                                                        if (dictionary.ContainsKey(current.Nodes[1]))
                                                        {
                                                            num3 = 42555095;
                                                            num5 = num3;
                                                        }
                                                        else
                                                        {
                                                            num3 = 1235605517;
                                                            num5 = num3;
                                                        }
                                                        continue;
                                                    }
                                                case 6u:
                                                    dictionary.Add(current.Nodes[1], Vector3.Distance(MeshFilter.mesh.vertices[current.Nodes[1]], _firstHit.point));
                                                    num3 = ((int)(num2 * 1087080439) ^ -97373029);
                                                    continue;
                                            }
                                            break;
                                        }
                                    }
                                    end_IL_0076:;
                                }
                                KeyValuePair<int, float> keyValuePair = (from x in dictionary
                                                                         orderby x.Value
                                                                         select x).First();
                                while (true)
                                {
                                    int num7 = 618151660;
                                    while (true)
                                    {
                                        switch ((num2 = (uint)(num7 ^ 0x63FB4D19)) % 4u)
                                        {
                                            case 0u:
                                                break;
                                            case 1u:
                                                key = keyValuePair.Key;
                                                num7 = (int)((num2 * 161441052) ^ 0x430FD37B);
                                                continue;
                                            case 2u:
                                                dictionary.Clear();
                                                num7 = ((int)(num2 * 454209606) ^ -1511228610);
                                                continue;
                                            default:
                                                {
                                                    using (List<Node>.Enumerator enumerator2 = _nodes.GetEnumerator())
                                                    {
                                                        while (true)
                                                        {
                                                            int num8;
                                                            int num9;
                                                            if (!enumerator2.MoveNext())
                                                            {
                                                                num8 = 1378908885;
                                                                num9 = num8;
                                                            }
                                                            else
                                                            {
                                                                num8 = 356640434;
                                                                num9 = num8;
                                                            }
                                                            while (true)
                                                            {
                                                                switch ((num2 = (uint)(num8 ^ 0x63FB4D19)) % 7u)
                                                                {
                                                                    default:
                                                                        goto end_IL_0275;
                                                                    case 4u:
                                                                        goto end_IL_0275;
                                                                    case 3u:
                                                                        num8 = 356640434;
                                                                        continue;
                                                                    case 5u:
                                                                        {
                                                                            int num11;
                                                                            if (!dictionary.ContainsKey(current2.Nodes[1]))
                                                                            {
                                                                                num8 = 1738583861;
                                                                                num11 = num8;
                                                                            }
                                                                            else
                                                                            {
                                                                                num8 = 488635010;
                                                                                num11 = num8;
                                                                            }
                                                                            continue;
                                                                        }
                                                                    case 6u:
                                                                        break;
                                                                    case 1u:
                                                                        {
                                                                            current2 = enumerator2.Current;
                                                                            int num10;
                                                                            if (dictionary.ContainsKey(current2.Nodes[0]))
                                                                            {
                                                                                num8 = 856510164;
                                                                                num10 = num8;
                                                                            }
                                                                            else
                                                                            {
                                                                                num8 = 647066314;
                                                                                num10 = num8;
                                                                            }
                                                                            continue;
                                                                        }
                                                                    case 0u:
                                                                        dictionary.Add(current2.Nodes[1], Vector3.Distance(MeshFilter.mesh.vertices[current2.Nodes[1]], _secondHit.point));
                                                                        num8 = (((int)num2 * -99398846) ^ -785856038);
                                                                        continue;
                                                                    case 2u:
                                                                        dictionary.Add(current2.Nodes[0], Vector3.Distance(MeshFilter.mesh.vertices[current2.Nodes[0]], _secondHit.point));
                                                                        num8 = (int)((num2 * 1601032032) ^ 0x5847C2F4);
                                                                        continue;
                                                                }
                                                                break;
                                                            }
                                                        }
                                                        end_IL_0275:;
                                                    }
                                                    KeyValuePair<int, float> keyValuePair2 = (from x in dictionary
                                                                                              orderby x.Value
                                                                                              select x).First();
                                                    while (true)
                                                    {
                                                        int num12 = 101644536;
                                                        while (true)
                                                        {
                                                            switch ((num2 = (uint)(num12 ^ 0x63FB4D19)) % 6u)
                                                            {
                                                                case 3u:
                                                                    break;
                                                                case 4u:
                                                                    stopwatch.Start();
                                                                    list2 = _dijkstras.CalculatePath(key, key2);
                                                                    stopwatch.Stop();
                                                                    _calcDuration.text = "Calc Duration: " + stopwatch.Elapsed.TotalMilliseconds.ToString() + " ms";
                                                                    num12 = (int)((num2 * 414991800) ^ 0x771250BB);
                                                                    continue;
                                                                case 2u:
                                                                    stopwatch = new Stopwatch();
                                                                    num12 = ((int)(num2 * 688711528) ^ -573737743);
                                                                    continue;
                                                                case 0u:
                                                                    _nodeCount.text = "NodeCount:"  + list2.Count;
                                                                    num12 = (int)((num2 * 288667062) ^ 0x22790C48);
                                                                    continue;
                                                                case 5u:
                                                                    key2 = keyValuePair2.Key;
                                                                    num12 = (((int)num2 * -726624076) ^ -1809353299);
                                                                    continue;
                                                                default:
                                                                    {
                                                                        LineRenderer component = GetComponent<LineRenderer>();
                                                                        List<Vector3> list = new List<Vector3>();
                                                                        using (List<int>.Enumerator enumerator3 = list2.GetEnumerator())
                                                                        {
                                                                            while (true)
                                                                            {
                                                                                int num13;
                                                                                int num14;
                                                                                if (!enumerator3.MoveNext())
                                                                                {
                                                                                    num13 = 713851669;
                                                                                    num14 = num13;
                                                                                }
                                                                                else
                                                                                {
                                                                                    num13 = 898301328;
                                                                                    num14 = num13;
                                                                                }
                                                                                while (true)
                                                                                {
                                                                                    switch ((num2 = (uint)(num13 ^ 0x63FB4D19)) % 5u)
                                                                                    {
                                                                                        default:
                                                                                            goto end_IL_0544;
                                                                                        case 0u:
                                                                                            goto end_IL_0544;
                                                                                        case 4u:
                                                                                            num13 = 898301328;
                                                                                            continue;
                                                                                        case 2u:
                                                                                            current3 = enumerator3.Current;
                                                                                            num13 = 1776768705;
                                                                                            continue;
                                                                                        case 3u:
                                                                                            break;
                                                                                        case 1u:
                                                                                            list.Add(MeshFilter.mesh.vertices[current3]);
                                                                                            num13 = (((int)num2 * -2913219) ^ -1024639669);
                                                                                            continue;
                                                                                    }
                                                                                    break;
                                                                                }
                                                                            }
                                                                            end_IL_0544:;
                                                                        }
                                                                        component.positionCount = list.Count;
                                                                        component.SetPositions(list.ToArray());
                                                                        return;
                                                                    }
                                                            }
                                                            break;
                                                        }
                                                    }
                                                }
                                        }
                                        break;
                                    }
                                }
                            }
                    }
                    break;
                }
                continue;
                IL_0063:
                num = 1063615677;
                goto IL_000d;
            }
        }
    }

    static Mesh _202A_202B_200F_206B_202B_202E_202C_200B_200D_206A_202A_202D_200F_202B_206D_200F_200C_200F_202A_200C_200E_202E_200B_200D_202D_200E_200B_202B_202D_206C_206A_206D_202E_202C_206E_202E_202E_200B_206D_200F_202E(MeshFilter P_0)
    {
        return P_0.mesh;
    }

    static int[] _202B_202E_206D_202C_206C_206D_200B_200D_202B_200B_206F_206F_200B_200C_206F_202B_206F_206A_202B_200D_200F_202C_206B_202B_206B_202D_206A_206D_200F_206C_206A_206F_202B_206D_202E_202E_200C_200C_202E_200F_202E(Mesh P_0)
    {
        return P_0.triangles;
    }

    static Vector3[] _200D_202C_206E_206E_202E_202A_202A_202E_206C_206C_202A_206C_202C_202D_200D_200E_202C_206E_200F_206F_202B_206D_200E_202B_200D_202D_202D_206B_200B_200D_200E_206F_202D_202E_200F_200C_202B_200B_202A_206F_202E(Mesh P_0)
    {
        return P_0.vertices;
    }

    static bool _202D_202E_206F_202D_206C_206C_206C_202C_200F_206F_206C_202D_202E_200B_202E_202E_200C_206C_200C_206D_206D_206C_202D_200E_206F_206A_206B_206C_206E_202A_200B_200B_202B_202D_200E_202E_200E_206C_206C_202C_202E(UnityEngine.Object P_0, UnityEngine.Object P_1)
    {
        return P_0 != P_1;
    }

    static void _206A_206C_206A_200E_206F_200C_202A_200F_206E_206E_202C_206B_206B_206C_202A_206E_202A_200D_200D_202B_202E_200F_200C_206E_200E_206C_200F_206E_202A_202D_206C_206E_206D_202B_202A_200D_206C_200C_200D_200F_202E(GameObject P_0, bool P_1)
    {
        P_0.SetActive(P_1);
    }

    static Camera _206A_206E_206B_200E_206F_202E_200D_200D_206D_206F_202B_200B_200F_200E_200C_200D_202E_206E_200F_202B_200B_200D_200B_200E_200C_202B_206C_200F_200E_206B_200F_202A_200F_202E_200C_206E_200C_206B_200C_202E_202E()
    {
        return Camera.main;
    }

    static Vector3 _206C_206F_200F_200C_202C_206C_206A_206E_206F_202B_200E_200C_200F_202C_202A_200C_202A_202C_202C_206E_206D_200C_206D_202C_206F_200B_202A_206D_202B_206E_206D_202B_206C_200C_200B_206A_202D_200B_202C_202E()
    {
        return UnityEngine.Input.mousePosition;
    }

    static Ray _206B_200F_202E_202B_200B_206E_206A_206E_200E_200B_200D_200C_206F_200B_202A_202D_202D_200D_200E_200B_206B_200E_206C_206A_202E_202B_206B_206A_200F_200C_202C_206D_206F_200D_202E_202C_202A_202B_200F_200E_202E(Camera P_0, Vector3 P_1)
    {
        return P_0.ScreenPointToRay(P_1);
    }

    static bool _202D_206E_206F_206E_202A_202A_202C_200C_200F_200E_200C_206F_206A_200C_200D_202A_200B_206C_200F_202B_200C_200E_200C_206B_200F_206E_206F_200B_200F_202D_200B_202C_202B_206F_200C_206F_206B_200F_206D_206C_202E(Ray P_0, ref RaycastHit P_1, float P_2)
    {
        return Physics.Raycast(P_0, out P_1, P_2);
    }

    static bool _200B_206C_206F_202D_206D_206E_206C_202A_200F_202D_206C_206A_206C_202E_202A_202D_206D_200C_206C_206A_202E_200F_206F_206C_206A_202B_206A_206F_202E_202C_202B_202E_202D_200F_200C_202B_206E_200E_200C_202D_202E(int P_0)
    {
        return Input.GetMouseButtonDown(P_0);
    }

    static Transform _206F_200F_206E_200B_200C_200C_206A_206A_200B_206A_202D_206F_202C_206E_206B_202C_206E_202E_202A_202C_206E_206D_200E_202A_206E_202B_206E_202A_200E_200E_206E_202D_200F_202B_200E_200F_202D_202C_206E_202D_202E(GameObject P_0)
    {
        return P_0.transform;
    }
}
